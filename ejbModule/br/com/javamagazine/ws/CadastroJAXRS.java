package br.com.javamagazine.ws;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import br.com.javamagazine.ejb.CadastroEJB;
import br.com.javamagazine.pojo.Livro;

@Stateless(name="cadastro")
@Path("/cadastro")
@DeclareRoles({"usuarios","administradores"})
@Produces({"application/json; charset=UTF-8"})
public class CadastroJAXRS {

	@EJB
	private CadastroEJB cadastro;
	
	@GET
	@Path("/buscar/{id}")	
	public Livro buscarPorId(@PathParam("id") long id) {
		return cadastro.buscarPorId(id);
	}
	
	@GET 
	@Path("/listar")
	public List<Livro> listar() {
		return cadastro.listar();
	}
	
	@POST
	@Path("/salvar/{id}")	
	@RolesAllowed({"usuarios","administradores"})
	public Response salvar(@PathParam("id") long id, @FormParam("titulo") String titulo) {
		Livro livro = new Livro(id, titulo);
		cadastro.salvar(livro);
		return Response.status(200).entity(livro).build();
	}

	@DELETE
	@Path("/excluir/{id}")	
	@RolesAllowed("administradores")
	public Response excluir(@PathParam("id") long id) {
		cadastro.excluir(id);
		return Response.status(200).build();
	}
}