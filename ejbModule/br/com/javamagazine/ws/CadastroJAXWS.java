package br.com.javamagazine.ws;

import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import br.com.javamagazine.ejb.CadastroEJB;
import br.com.javamagazine.pojo.Livro;

@Stateless(name="cadastro")
@DeclareRoles({"usuarios","administradores"})
@WebService(portName = "cadastroPort",
	serviceName = "cadastroService",
	targetNamespace = "http://devmedia.com.br/wsdl",
	endpointInterface = "br.com.javamagazine.ws.Cadastro")
public class CadastroJAXWS implements Cadastro {
	
	@EJB
	private CadastroEJB cadastro;

	public Livro buscarPorId(long id) {
		return cadastro.buscarPorId(id);
	}

	public List<Livro> listar() {
		return cadastro.listar();
	}
	
	@RolesAllowed({"usuarios","administradores"})
	public void salvar(long id, String titulo) {
		cadastro.salvar(new Livro(id, titulo));
	}

	@RolesAllowed("administradores")
	public void excluir(long id) {
		cadastro.excluir(id);
	}
}