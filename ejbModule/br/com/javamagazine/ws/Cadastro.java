package br.com.javamagazine.ws;

import java.util.List;

import javax.jws.WebService;

import br.com.javamagazine.pojo.Livro;

@WebService
public interface Cadastro {
	void salvar(long id, String titulo);
	void excluir(long id);
	Livro buscarPorId(long id);
	List<Livro> listar();
}