package br.com.javamagazine.ejb;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.com.javamagazine.pojo.Livro;

@Local
@Stateless
public class CadastroEJB {

	@PersistenceContext(unitName = "cadastro")
	private EntityManager em;

	public Livro buscarPorId(long id) {
		return em.find(Livro.class, id);
	}

	public void excluir(long id) {
		Livro livro = em.find(Livro.class, id);
		em.remove(livro);
	}

	@SuppressWarnings("unchecked")
	public List<Livro> listar() {
		Query query = em.createQuery("select l from Livro as l");
		return query.getResultList();
	}
	
	public void salvar(String titulo) {
		salvar(new Livro(titulo));
	}

	public void salvar(Livro livro) {
		if (livro.getId() > 0) {
			em.merge(livro);
		} else {
			em.persist(livro);
		}
	}
}